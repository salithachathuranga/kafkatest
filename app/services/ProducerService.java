package services;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class ProducerService {

    public static String TOPIC = "test-kafka-topic";

    public Properties setConfigs() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return properties;
    }

    public Producer<String, String> createProducer() {
        return new KafkaProducer<>(setConfigs());
    }

    public void postMessageToKafka(JsonNode body) {
        ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, body.get("key").asText(), body.get("value").asText());
        Producer<String, String> producer = createProducer();
        producer.send(record);
        producer.close();
    }
}
