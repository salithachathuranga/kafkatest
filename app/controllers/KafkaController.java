package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import org.apache.kafka.clients.producer.KafkaProducer;
import play.libs.Json;
import play.mvc.*;
import services.ConsumerService;
import services.ProducerService;

public class KafkaController extends Controller {

    @Inject
    public ProducerService producerService;

    @Inject
    public ConsumerService consumerService;

    public Result postMessage(Http.Request request) {
        JsonNode body = request.body().asJson();
        producerService.postMessageToKafka(body);
        return ok(Json.toJson(body));
    }

    public Result subscribeMessage() {
        consumerService.subscribeMessageFromKafka();
        return ok("done");
    }

}
