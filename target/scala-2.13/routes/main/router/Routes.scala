// @GENERATOR:play-routes-compiler
// @SOURCE:/home/pool/Desktop/kafka-test/conf/routes
// @DATE:Tue Apr 07 22:17:49 IST 2020

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:2
  KafkaController_1: controllers.KafkaController,
  // @LINE:7
  Assets_0: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:2
    KafkaController_1: controllers.KafkaController,
    // @LINE:7
    Assets_0: controllers.Assets
  ) = this(errorHandler, KafkaController_1, Assets_0, "/")

  def withPrefix(addPrefix: String): Routes = {
    val prefix = play.api.routing.Router.concatPrefix(addPrefix, this.prefix)
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, KafkaController_1, Assets_0, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """publish""", """controllers.KafkaController.postMessage(request:Request)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """subscribe""", """controllers.KafkaController.subscribeMessage()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:2
  private[this] lazy val controllers_KafkaController_postMessage0_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("publish")))
  )
  private[this] lazy val controllers_KafkaController_postMessage0_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      KafkaController_1.postMessage(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.KafkaController",
      "postMessage",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """publish""",
      """""",
      Seq()
    )
  )

  // @LINE:3
  private[this] lazy val controllers_KafkaController_subscribeMessage1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("subscribe")))
  )
  private[this] lazy val controllers_KafkaController_subscribeMessage1_invoker = createInvoker(
    KafkaController_1.subscribeMessage(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.KafkaController",
      "subscribeMessage",
      Nil,
      "GET",
      this.prefix + """subscribe""",
      """""",
      Seq()
    )
  )

  // @LINE:7
  private[this] lazy val controllers_Assets_versioned2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned2_invoker = createInvoker(
    Assets_0.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:2
    case controllers_KafkaController_postMessage0_route(params@_) =>
      call { 
        controllers_KafkaController_postMessage0_invoker.call(
          req => KafkaController_1.postMessage(req))
      }
  
    // @LINE:3
    case controllers_KafkaController_subscribeMessage1_route(params@_) =>
      call { 
        controllers_KafkaController_subscribeMessage1_invoker.call(KafkaController_1.subscribeMessage())
      }
  
    // @LINE:7
    case controllers_Assets_versioned2_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned2_invoker.call(Assets_0.versioned(path, file))
      }
  }
}
