// @GENERATOR:play-routes-compiler
// @SOURCE:/home/pool/Desktop/kafka-test/conf/routes
// @DATE:Tue Apr 07 22:17:49 IST 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
