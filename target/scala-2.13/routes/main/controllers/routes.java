// @GENERATOR:play-routes-compiler
// @SOURCE:/home/pool/Desktop/kafka-test/conf/routes
// @DATE:Tue Apr 07 22:17:49 IST 2020

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseKafkaController KafkaController = new controllers.ReverseKafkaController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseKafkaController KafkaController = new controllers.javascript.ReverseKafkaController(RoutesPrefix.byNamePrefix());
  }

}
