// @GENERATOR:play-routes-compiler
// @SOURCE:/home/pool/Desktop/kafka-test/conf/routes
// @DATE:Tue Apr 07 22:17:49 IST 2020

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:2
package controllers {

  // @LINE:7
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:2
  class ReverseKafkaController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:2
    def postMessage(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "publish")
    }
  
    // @LINE:3
    def subscribeMessage(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "subscribe")
    }
  
  }


}
